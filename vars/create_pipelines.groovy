def call(String name = "generated", String script = ) {
  jobDsl scriptText: '''pipelineJob(\'${name}\') {
    definition {
      cps {
        script(readFileFromWorkspace("script.groovy"))
        sandbox()
      }
    }
  }'''
}
