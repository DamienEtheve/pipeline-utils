import groovy.json.JsonSlurper

def call() {
  def list = null
  // load file
  def inputFile = new File("${workspace}/array.json")
  println(inputFile.text)

  // parse response
  def jsonSlurper = new JsonSlurper()
  list  = jsonSlurper.parseText(inputFile.text)
  return list
}