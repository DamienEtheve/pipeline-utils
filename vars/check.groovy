def call(String param = "default_value") {
  echo "params: ${params}"
  echo "env: ${env}"
  echo "workspace: ${workspace}"
  sh "node --version && npm --version && yarn --version && tsc --version && ts-node --version"
  echo "${param}"
}
