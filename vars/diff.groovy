/*
  author      : Damien ETHÈVE
  description : si l'un des fichiers specifié existe alors retourne true sinon retourne false
  usage       : def result = diff("regex")
  examples    : 
    script {
      def isDiff  = diff("fileNotEdited|fileEdited")  // > true
      def isDiff2 = diff("fileNotEdited")             // > false
      def isDiff3 = diff("fileEdited")                // > true

      // all sub directory
      def isDiff3 = diff("dir/[^/]+/fileEdited")         // > true
      def isDiff3 = diff("dir/[^/]+/fileNotEdited")      // > false
    }
*/

def call(String expr = "default_value") {
  def state = sh(
      script : "git diff --name-only HEAD | grep -E '${expr}'",
      returnStatus : true
  )
  return (state == 0) ? true : false
}
